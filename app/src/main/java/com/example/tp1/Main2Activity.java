package com.example.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Button;
import android.view.View;
import android.widget.Toast;




public class Main2Activity extends AppCompatActivity {

    Toolbar mToolbar;
    ImageView mImageView;
    EditText cap;
    EditText lan;
    EditText mon;
    EditText pop;
    EditText sup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        String text = getIntent().getStringExtra("val");

        final TextView t1 = (TextView)findViewById(R.id.CountryNameSecond);
        t1.setText(text);
        //mToolbar = (Toolbar) findViewById(R.id.toolbar2);
        mImageView = (ImageView) findViewById(R.id.imageView2);
        //sup = (EditText) findViewById(R.id.edit5);
        //pop = (EditText) findViewById(R.id.edit4);
        //mon = (EditText) findViewById(R.id.edit3);
        //lan = (EditText) findViewById(R.id.edit2);
        //cap = (EditText) findViewById(R.id.edit1);

        Bundle mBundle = getIntent().getExtras();

            //mToolbar.setTitle(mBundle.getString("countryName"));
            mImageView.setImageResource(mBundle.getInt("countryFlag"));
            //cap.setText(mBundle.getString("mCapital"));
            //lan.setText(mBundle.getString("mLanguage"));
            //mon.setText(mBundle.getString("mCurrency"));
            //pop.setText(mBundle.getString("mPopulation"));
            //sup.setText(mBundle.getString("mArea"));

        final EditText e1 = (EditText)findViewById(R.id.edit1);
        final EditText e2 = (EditText)findViewById(R.id.edit2);
        final EditText e3 = (EditText)findViewById(R.id.edit3);
        final EditText e4 = (EditText)findViewById(R.id.edit4);
        final EditText e5 = (EditText)findViewById(R.id.edit5);
        e1.setText(getIntent().getStringExtra("capitale"));
        e2.setText(getIntent().getStringExtra("langue"));
        e3.setText(getIntent().getStringExtra("monnaie"));
        e4.setText(Integer.toString(getIntent().getIntExtra("population",0)));
        e5.setText(Integer.toString(getIntent().getIntExtra("superficie",0)));

        Button b1 = findViewById(R.id.sauvegarder);
        b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                e1.setText(e1.getText());
                e2.setText(e2.getText());
                e3.setText(e3.getText());
                e4.setText(e4.getText());
                e5.setText(e5.getText());
                Country object = CountryList.getCountry(String.valueOf(t1.getText()));
                object.setmCapital(String.valueOf(e1.getText()));
                object.setmLanguage(String.valueOf(e2.getText()));
                object.setmCurrency(String.valueOf(e3.getText()));
                object.setmPopulation((Integer.parseInt(String.valueOf(e4.getText()))));
                object.setmArea((Integer.parseInt(String.valueOf(e5.getText()))));
                Toast.makeText(getApplication(),"Sauvegarde réussie ",Toast.LENGTH_LONG).show();


            }
        });
    }
}