package com.example.tp1;
import android.content.Intent;
import android.support.v7.widget.Toolbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    Adapter adapter;
   // Toolbar mToolbar;
    ListView mListView;
    int[] countryFlags = {
            R.drawable.flag_of_germany,
            R.drawable.flag_of_japan,
            R.drawable.flag_of_the_united_states,
            R.drawable.flag_of_france,
            R.drawable.flag_of_spain,
            R.drawable.flag_of_south_africa

    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mListView = findViewById(R.id.listview);
        final Adapter adapter = new Adapter(MainActivity.this, Arrays.asList(CountryList.getNameArray()),countryFlags);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent mIntent = new Intent(MainActivity.this, Main2Activity.class);
                mIntent.putExtra("countryFlag", countryFlags[position]);
                String value = (String) adapter.getItem(position);
                Country country = CountryList.getCountry(value);
                String image = country.getmImgFile();
                String capitale= country.getmCapital();
                String langue=country.getmLanguage();
                String monnaie = country.getmCurrency();
                int population = country.getmPopulation();
                int superficie = country.getmArea();
                //Intent myintent = new Intent(view.getContext(), Main2Activity.class);
                mIntent.putExtra("val",value);
                mIntent.putExtra("imageUrl",image);
                mIntent.putExtra("capitale",capitale);
                mIntent.putExtra("langue",langue);
                mIntent.putExtra("monnaie",monnaie);
                mIntent.putExtra("population",population);
                mIntent.putExtra("superficie",superficie);


                startActivity(mIntent);
            }
        });
    }


}